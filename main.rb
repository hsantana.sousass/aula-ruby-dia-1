require "json"

file = File.read("./pokemons.json")
POKEMONS = JSON.parse(file)

# Ler e imprimir quantos pokémons distintos existem no arquivo(Usar só o primeiro nome que está antes do hífen. Ex. raichu-alola = raichu) 
def quantidade_de_pokemons_distintos 
    pokemons_distitos = []
    cont = 0
    POKEMONS["results"].each do |nome|
        nome = nome["name"].split('-')[0]
        unless pokemons_distitos.include? nome
            pokemons_distitos.push(nome)
            cont += 1
        end 
    end

    return cont, pokemons_distitos
    # puts quantidade_de_pokemons_distintos
end

# Quais são os 10 pokémons com maior ocorrência segundo o primeiro nome
# lógica de ordenação do buble sort
def ordenar_array array, hash 
    tamanho = array.length
    inicio = 1
    fim = tamanho
    for i in 0...tamanho do
        for y in inicio...fim do
            if (hash[array[y]] < hash[array[y-1]])
                aux = array[y-1]
                array[y-1] = array[y]
                array[y] = aux
            end
        end
        fim -=1
    end
    array
end


def obtem_maiores_ocorrencia hash, chaves, numero
    pokemons_maior_ocorrencia = []
    n = 0
    chaves.each do |chave|
        qt_pokemon = hash[chave]
        ordenar_array(pokemons_maior_ocorrencia, hash)
        if n < numero
            pokemons_maior_ocorrencia.push(chave)
            n +=1
        elsif qt_pokemon > hash[pokemons_maior_ocorrencia[0]]
            pokemons_maior_ocorrencia[0] = chave
        end
    end
    
    pokemons_maior_ocorrencia
end


def obtem_maiores_ocorrencia_pokemons numero
    pokemons_nome = []
    POKEMONS["results"].each do |nome|
        pokemons_nome.push(nome["name"].split('-')[0])
    end
    # hash com o número de ocorrência  de cada pokemon
    n_ocorrencias = pokemons_nome.tally
    chaves = n_ocorrencias.keys
    obtem_maiores_ocorrencia(n_ocorrencias, chaves, numero)
end


def obtem_maiores_ocorrencia_das_iniciais_pokemon numero
    iniciais_pokemon = []
    quantidade_de_pokemons_distintos[1].each do |pokemon|
        iniciais_pokemon.push(pokemon[0])
    end
    
    pokemon_ocorrencia_letra_inicial = iniciais_pokemon.tally
    
    obtem_maiores_ocorrencia(pokemon_ocorrencia_letra_inicial, pokemon_ocorrencia_letra_inicial.keys, numero)
end

puts quantidade_de_pokemons_distintos[0]
puts
print obtem_maiores_ocorrencia_pokemons 10
puts "\n\n"
print obtem_maiores_ocorrencia_das_iniciais_pokemon 5